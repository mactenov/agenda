#MACTENOVA#
# CodeIgniter 2
Este es un template basado en Codeigniter 2, agradecemos a los autores correspondientes de tanto las librerias como frameworks utilizados.

**Notas**
El framework sigue una estructura basada en componentes, para iniciar es importante lanzar los siguientes comandos desde la terminal del proyecto una vez clonado.
# npm install
    -> Obtendras las nuevas versiones de las librerias.
# npm run build
    -> Ejecutara una script de BabelJs para compilar ES6 a una version antigua de JS compatible con navegadores viejos.
    -> Cada vez que se guarda un archivo JS se autogeneran los archivos y se compilan; NO es necesario crearlos en ambas carpetas.
    -> No es necesario volver a correr el comando, amenos que se cierre la terminal.
    -> En entorno Dev, los Js se crean en la carpeta js/ES6 y se compilan automaticamente en js/prod, todas las referencias deben referir a esa carpeta <script src="../js/prod/file.js"> </scritp>

**Actualizaciones**
*3/Marzo/2019 -> Instalacion de librerias*
