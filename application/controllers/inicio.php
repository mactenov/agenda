<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inicio extends General {
 public function __construct(){
    parent::__construct();

 }
 function index(){
    $this->load->view("welcome_message");
 }
 function getData(){
     $nombre = $this->data_json['nombre'];
     $apellido = $this->data_json['apellido'];
     echo $nombre.'<br>'.$apellido;
 }
}