<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class General extends CI_Controller {
    protected $info_usuario;
	protected $layout='base';
	protected $configuraciones;
	protected $show_layout = true;
	protected $permisos;
	protected $id_usuario;
    protected $data_post;
	protected $data_json;
    public function __construct(){
            parent::__construct();
			$this->load->library('session');
			// $login = $this->verificar_sesion();
			// if($login === FALSE){
			// 	redirect('login');
			// }else{
				$this->permisos = $this->session->userdata("datos_usuario")['permiso'];
				$this->id_usuario = $this->session->userdata("datos_usuario")['id_usuario'];
				$this->data_post = file_get_contents("php://input");
				  $this->data_json = json_decode($this->data_post,true);
				  //json_decode => TRUE $dato['parametro'] o FALSE $dato->parametro
			//}
    }
    protected function set_layout($nuevo_layout = NULL){
		if($nuevo_layout != NULL)
			$this->layout = $nuevo_layout;
		$show_layout = TRUE;
	}
    protected function view($vista, $param = NULL, $view_as_data= FALSE){
        $view_user_data = array();
		//se envia el permiso a la visa para un mejor control
		if($this->show_layout == true){
			$data = array('vista' => $vista, 'vista_params' => $param, 'view_user_data' => $view_user_data,);
			return $this->load->view('/layout_index/'.$this->layout, $data, $view_user_data);
		}else{
			return $this->load->view('/vistas/'.$vista, $param, $data);
		}
    }
    protected function verificar_sesion(){
		if ($this->session->userdata('login_state') !== FALSE){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	function mensaje($titulo,$texto,$alerta){
		$mensaje = array(
			"titulo" => $titulo,
			"texto" => $texto,
			"alerta" => $alerta
		);
		return json_encode($mensaje);
	}

	function response($data, $estatus = 200){
        $this->output->set_status_header($estatus);
        $this->output
    ->set_content_type('application/json')
    ->set_output(json_encode($data));
    }
}