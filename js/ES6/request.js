class Request {
    constructor(){

    }
    /**
     * 
     * @param {*} metodo controlador/funcion
     * @param {*} params JSON.stringify({})
     */
    post(metodo,params){
        let promise = new Promise((resolve,reject)=>{
            $.ajax({
                url: CI_ROOT + metodo,
                type:'POST',
                processData:false,
                contentType:false,
                cache:false,
                data: params,
                datatype:"json",
                success: function(response,textStatus,JQxhr){
                    resolve(response);
                },error:function(JQxhr,textStatus,errorThrown){
                    swal("Alerta","Ocurrio algo inesperado, intente mas tarde","warning");
                    reject(textStatus);
                  }
                });
        });
        return promise;
    }
}
const req = new Request();