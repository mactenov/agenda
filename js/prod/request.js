"use strict";

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var Request =
/*#__PURE__*/
function () {
  function Request() {
    _classCallCheck(this, Request);
  }
  /**
   * 
   * @param {*} metodo controlador/funcion
   * @param {*} params JSON.stringify({})
   */


  _createClass(Request, [{
    key: "post",
    value: function post(metodo, params) {
      var promise = new Promise(function (resolve, reject) {
        $.ajax({
          url: CI_ROOT + metodo,
          type: 'POST',
          processData: false,
          contentType: false,
          cache: false,
          data: params,
          datatype: "json",
          success: function success(response, textStatus, JQxhr) {
            resolve(response);
          },
          error: function error(JQxhr, textStatus, errorThrown) {
            swal("Alerta", "Ocurrio algo inesperado, intente mas tarde", "warning");
            reject(textStatus);
          }
        });
      });
      return promise;
    }
  }]);

  return Request;
}();

var req = new Request();