-- Adminer 4.7.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

CREATE DATABASE `agenda_db` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */;
USE `agenda_db`;

DROP TABLE IF EXISTS `agenda`;
CREATE TABLE `agenda` (
  `id_agenda` int(11) NOT NULL AUTO_INCREMENT,
  `id_doctor` int(11) NOT NULL,
  `id_paciente` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `fecha_hora` datetime NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_agenda`),
  KEY `id_doctor` (`id_doctor`),
  KEY `id_paciente` (`id_paciente`),
  KEY `id_servicio` (`id_servicio`),
  CONSTRAINT `agenda_ibfk_1` FOREIGN KEY (`id_doctor`) REFERENCES `doctor` (`id_doctor`) ON DELETE RESTRICT,
  CONSTRAINT `agenda_ibfk_2` FOREIGN KEY (`id_servicio`) REFERENCES `servicio` (`id_servicio`) ON DELETE RESTRICT,
  CONSTRAINT `agenda_ibfk_3` FOREIGN KEY (`id_doctor`) REFERENCES `doctor` (`id_doctor`) ON DELETE RESTRICT,
  CONSTRAINT `agenda_ibfk_4` FOREIGN KEY (`id_paciente`) REFERENCES `paciente` (`id_paciente`) ON DELETE RESTRICT,
  CONSTRAINT `agenda_ibfk_5` FOREIGN KEY (`id_servicio`) REFERENCES `servicio` (`id_servicio`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `agenda_medicamento`;
CREATE TABLE `agenda_medicamento` (
  `id_agenda` int(11) NOT NULL,
  `id_medicamento` int(11) NOT NULL,
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `id_agenda` (`id_agenda`),
  KEY `id_medicamento` (`id_medicamento`),
  CONSTRAINT `agenda_medicamento_ibfk_1` FOREIGN KEY (`id_agenda`) REFERENCES `agenda` (`id_agenda`) ON DELETE RESTRICT,
  CONSTRAINT `agenda_medicamento_ibfk_2` FOREIGN KEY (`id_medicamento`) REFERENCES `medicamento` (`id_medicamento`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `almacen`;
CREATE TABLE `almacen` (
  `id_almacen` int(11) NOT NULL AUTO_INCREMENT,
  `id_clinica` int(11) NOT NULL,
  `id_medicamento` int(11) NOT NULL,
  `entradas` int(11) NOT NULL,
  `salidas` int(11) NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_almacen`),
  KEY `id_clinica` (`id_clinica`),
  KEY `id_medicamento` (`id_medicamento`),
  CONSTRAINT `almacen_ibfk_1` FOREIGN KEY (`id_clinica`) REFERENCES `clinica` (`id_clinica`) ON DELETE RESTRICT,
  CONSTRAINT `almacen_ibfk_2` FOREIGN KEY (`id_medicamento`) REFERENCES `medicamento` (`id_medicamento`) ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `clinica`;
CREATE TABLE `clinica` (
  `id_clinica` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `domicilio` varchar(100) COLLATE utf8mb4_general_ci NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_clinica`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `doctor`;
CREATE TABLE `doctor` (
  `id_doctor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `apellido` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `telefono` varchar(13) COLLATE utf8mb4_general_ci NOT NULL,
  `correo` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `cedula` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_doctor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `medicamento`;
CREATE TABLE `medicamento` (
  `id_medicamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_general_ci NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_medicamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `paciente`;
CREATE TABLE `paciente` (
  `id_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `apellido` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `telefono` varchar(13) COLLATE utf8mb4_general_ci NOT NULL,
  `correo` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `seguro_social` varchar(20) COLLATE utf8mb4_general_ci NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


DROP TABLE IF EXISTS `servicio`;
CREATE TABLE `servicio` (
  `id_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(35) COLLATE utf8mb4_general_ci NOT NULL,
  `descripcion` text COLLATE utf8mb4_general_ci NOT NULL,
  `estatus` tinyint(1) NOT NULL DEFAULT '1',
  `fecha_registrado` timestamp NOT NULL ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_servicio`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


-- 2019-06-12 23:11:31
